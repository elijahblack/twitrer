alter table if exists message drop constraint if exists message_user_fk;
alter table if exists user_role drop constraint if exists user_role_user_fk;

drop table if exists message cascade;
drop table if exists user_role cascade;
drop table if exists usr cascade;

drop sequence if exists hibernate_sequence;

drop table if exists databasechangelog cascade;
drop table if exists databasechangeloglock cascade;

drop extension if exists pgcrypto;